﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CS481_HW6
{

    public class RestService
    {
        HttpClient _client;

        public RestService()
        {
            _client = new HttpClient();
        }

        /*method used to send GET request to the owlbot API. */


       public async Task<OwlbotData> GetOwlbotDataAsync(string uri) 
        {
            OwlbotData owlbotData = null;
            try
            {
                HttpResponseMessage response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    owlbotData = JsonConvert.DeserializeObject<OwlbotData>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return owlbotData;
        }
    }
}