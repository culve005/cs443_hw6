﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using Xamarin.Forms;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;

namespace CS481_HW6
{

    /*Reference: https://www.c-sharpcorner.com/article/check-network-connectivity-in-xamarin-forms-application-for-android-and-uwp/ 
     * This reference was used to work with the plugin connectivity. 
     *
     * Reference: https://docs.microsoft.com/en-us/xamarin/get-started/tutorials/web-service/?tutorial-step=1&tabs=vswin
     * This reference was used to work with Newtonsoft.Json
     */
    [DesignTimeVisible(false)]


    public partial class MainPage : ContentPage
    {
        
        RestService _restService;
        public MainPage()
        {
            InitializeComponent();
        }

        //On the appearence of the MainPage the connectivity is checked and a new RsetService object is initialized.
        protected override void OnAppearing()
        {
            ConNet.Text = CrossConnectivity.Current.ConnectionTypes.First().ToString();
            CrossConnectivity.Current.ConnectivityChanged += UpdateNetworkInfo;
            _restService = new RestService();
        }

        //Method to bind the context on the xaml 
        async void OnButtonClicked(object sender, EventArgs e)
        {
            //retreive data from Owlbotdata. Check that information from user is in the form of a string
            if (!string.IsNullOrWhiteSpace(wordEntry.Text))
            {
                OwlbotData owlbotData = await _restService.GetOwlbotDataAsync(GenerateRequestUri(Constants.OpenOwlbotEndpoint));
                BindingContext = owlbotData;
            }
        }

        //generates the requested uri
        string GenerateRequestUri(string endpoint)
        {
            string requestUri = endpoint;
            requestUri += $"?q={wordEntry.Text}";
            requestUri += "&units=imperial"; // or units=metric
            requestUri += $"&APPID={Constants.OpenOwlbotAPIKey}";
            return requestUri;
        } 
        protected override void OnDisappearing()
        {
            CrossConnectivity.Current.ConnectivityChanged -= UpdateNetworkInfo;
        }

        //Updates the network information
        private void UpdateNetworkInfo(object sender, ConnectivityChangedEventArgs e)
        {
            var connectionType = CrossConnectivity.Current.ConnectionTypes.FirstOrDefault();
            ConNet.Text = connectionType.ToString();
        }
    }
}

