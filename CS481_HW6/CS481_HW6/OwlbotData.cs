﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;


/* Defines classes used to model the JSON data retrieved from the owlbot webservice. */

namespace CS481_HW6
{
   public  class OwlbotData
    {
        public class Rootobject
        {
            public Definition[] definitions { get; set; }
            public string word { get; set; }
            public string pronunciation { get; set; }
        }

        public class Definition
        {
            [JsonProperty("type")]
            public string type { get; set; }

            [JsonProperty("definition")]
            public string definition { get; set; }

            [JsonProperty("example")]
            public string example { get; set; }

            [JsonProperty("image_url")]
            public string image_url { get; set; }

            [JsonProperty("emoji")]
            public object emoji { get; set; }
        }

    }
}
