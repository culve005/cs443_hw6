﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;

namespace CS481_HW6
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts  
            CrossConnectivity.Current.ConnectivityChanged += HandleConnectivityChanged;

        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        void HandleConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            Type currentPage = this.MainPage.GetType();
            if (e.IsConnected && currentPage != typeof(MainPage)) this.MainPage = new MainPage();
            else if (!e.IsConnected && currentPage != typeof(MainPage)) this.MainPage = new MainPage();
        }
    }
}
